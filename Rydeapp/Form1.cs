﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rydeapp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }
        private void button1_Click(object sender, EventArgs e)
        {

            Console.WriteLine("Welcome to our Ride App");
            string From = textBox1.Text;
            string To = textBox2.Text;
            double BaseFare = 2.5;
            double DistanceCharge = 0.81;
            double ServiceFee = 1.75;
            double totalprice = 0.0;
            string rb;
            TimeSpan time = DateTime.Now.TimeOfDay;

            if (From == "")
            {
                MessageBox.Show("Please enter your location");
            }
            else if (To == "")
            {
                MessageBox.Show("Please enter your destination");
            }

            else if (From == "Fairview Mall" && To == "Tim Hortons")
            {
                if (radioButton1.Checked == true)
                {
                    rb = radioButton1.Text;

                            if ((time > new TimeSpan(10, 00, 00) && time < new TimeSpan(12, 00, 00)) ||
                            (time > new TimeSpan(16, 00, 00) && time < new TimeSpan(18, 00, 00)) ||
                            (time > new TimeSpan(20, 00, 00) && time < new TimeSpan(21, 00, 00)))
                            {
                                DistanceCharge = 0.81 + (0.81 * 0.2);
                                totalprice = 2.5 + (DistanceCharge * 1.2) + 1.75;
                            }
                            else
                            {
                                totalprice = 2.5 + (0.81 * 1.2) + 1.75;
                            }
                                 if (totalprice < 5.5)
                                {
                                    totalprice = 5.5;
                                }
                   MessageBox.Show(rb + " is confirmed!!" + "\n" + "To: " + To + "\n" + "From: " + From + "\n" + "Base Fare: $" + BaseFare.ToString() +
                       "\n" + "Disatnce Charge: $" + DistanceCharge.ToString() + "per km" + "\n" + "Service Fee: $" + ServiceFee.ToString() + "\n" + "Total: " + totalprice.ToString());
                 }
                
                 else if (radioButton2.Checked == true)
                    {
                            rb = radioButton2.Text;
                            if ((time > new TimeSpan(10, 00, 00) && time < new TimeSpan(12, 00, 00)) ||
                            (time > new TimeSpan(16, 00, 00) && time < new TimeSpan(18, 00, 00)) ||
                            (time > new TimeSpan(20, 00, 00) && time < new TimeSpan(21, 00, 00)))
                            {
                                DistanceCharge = 0.81 + (0.81 * 0.2);
                                totalprice = 2.75 + ((DistanceCharge + 0.93)*1.2 ) + 1.75;
                            }
                            else
                            {

                                totalprice = 2.75 + (0.93 * 1.2) + 1.75;
                            }
                                    if (totalprice < 5.5)
                                    {
                                        totalprice = 5.5;
                                    }
                    
                    MessageBox.Show(rb + " is confirmed!!" + "\n" + "To: " + To + "\n" + "From: " + From + "\n" + "Base Fare: $" + BaseFare.ToString() +
        "\n" + "Disatnce Charge: $" + DistanceCharge.ToString() + "per km" + "\n" + "Service Fee: $" + ServiceFee.ToString() + "\n" + "Total: " + totalprice.ToString());
                }

                else
                {
                    MessageBox.Show("Please select which type of ryde you want!!");
                }
                }
            else if (From == "275 Yorkland Blvd" && To == "CN Tower")
                {
                    if (radioButton1.Checked == true)
                    {
                        rb = radioButton1.Text;
                            if ((time > new TimeSpan(10, 00, 00) && time < new TimeSpan(12, 00, 00)) ||
                            (time > new TimeSpan(16, 00, 00) && time < new TimeSpan(18, 00, 00)) ||
                            (time > new TimeSpan(20, 00, 00) && time < new TimeSpan(21, 00, 00)))
                            {
                                DistanceCharge = 0.81 + (0.81 * 0.2);
                                totalprice = 2.5 + (DistanceCharge * 22.9 ) + 1.75;
                            }
                            else
                            {
                                totalprice = 2.5 + (0.81 * 22.9) + 1.75;
                            }
                                    if (totalprice < 5.5)
                                    {
                                        totalprice = 5.5;
                                    }
                       
                        MessageBox.Show(rb + " is confirmed!!" + "\n" + "To: " + To + "\n" + "From: " + From + "\n" + "Base Fare: $" + BaseFare.ToString() +
                "\n" + "Disatnce Charge: $" + DistanceCharge.ToString() + "per km" + "\n" + "Service Fee: $" + ServiceFee.ToString() + "\n" + "Total: " + totalprice.ToString());

                    }
                    else if (radioButton2.Checked == true)
                    {
                        rb = radioButton2.Text;
                            if ((time > new TimeSpan(10, 00, 00) && time < new TimeSpan(12, 00, 00)) ||
                           (time > new TimeSpan(16, 00, 00) && time < new TimeSpan(18, 00, 00)) ||
                           (time > new TimeSpan(20, 00, 00) && time < new TimeSpan(21, 00, 00)))
                            {
                                DistanceCharge = 0.81 + (0.81 * 0.2);
                                totalprice = 2.75 + ((DistanceCharge +0.93) *22.9) + 1.75;
                            }
                            else
                            {
                                totalprice = 2.75 + (0.93 * 22.9) + 1.75;
                            }
                                if (totalprice < 5.5)
                                {
                                    totalprice = 5.5;
                                }
                        MessageBox.Show(rb + " is confirmed!!" + "\n" + "To: " + To + "\n" + "From: " + From + "\n" + "Base Fare: $" + BaseFare.ToString() +
                  "\n" + "Disatnce Charge: $" + DistanceCharge.ToString() + "per km" + "\n" + "Service Fee: $" + ServiceFee.ToString() + "\n" + "Total: " + totalprice.ToString());
                    }
                    else
                    {
                        MessageBox.Show("Please select which type of ryde you want!!");
                    }}
            else
               {
                    MessageBox.Show("Invalid location!!!Please enter a valid location");
               }}
        
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Ryde pool is cheaper than Ryde direct.");
           
         }
    }
}
